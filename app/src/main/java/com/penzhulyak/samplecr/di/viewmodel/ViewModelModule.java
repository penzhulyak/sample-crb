package com.penzhulyak.samplecr.di.viewmodel;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.penzhulyak.samplecr.ui.chatroom.ChatRoomViewModel;
import com.penzhulyak.samplecr.ui.login.LoginViewModel;
import com.penzhulyak.samplecr.ui.main.MainViewModel;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Singleton
    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);

    @Binds
    @IntoMap
    @Singleton
    @ViewModelKey(MainViewModel.class)
    abstract ViewModel bindMainViewModel(MainViewModel mainViewModel);

    @Binds
    @IntoMap
    @Singleton
    @ViewModelKey(LoginViewModel.class)
    abstract ViewModel bindLoginViewModel(LoginViewModel loginViewModel);

    @Binds
    @IntoMap
    @Singleton
    @ViewModelKey(ChatRoomViewModel.class)
    abstract ViewModel bindChatRoomViewModel(ChatRoomViewModel chatRoomViewModel);
}
