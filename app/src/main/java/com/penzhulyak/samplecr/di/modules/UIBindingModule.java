package com.penzhulyak.samplecr.di.modules;

import com.penzhulyak.samplecr.di.scope.ActivityScope;
import com.penzhulyak.samplecr.di.scope.FragmentScope;
import com.penzhulyak.samplecr.ui.chatroom.ChatRoomFragment;
import com.penzhulyak.samplecr.ui.login.LoginFragment;
import com.penzhulyak.samplecr.ui.main.MainActivity;
import com.penzhulyak.samplecr.ui.main.MainFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class UIBindingModule {

    @ActivityScope
    @ContributesAndroidInjector
    abstract MainActivity bindMainActivity();

    @FragmentScope
    @ContributesAndroidInjector
    abstract MainFragment provideMainFragment();

    @FragmentScope
    @ContributesAndroidInjector
    abstract LoginFragment provideLoginFragment();

    @FragmentScope
    @ContributesAndroidInjector
    abstract ChatRoomFragment provideChatRoomFragment();

}
