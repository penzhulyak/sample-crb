package com.penzhulyak.samplecr.di.modules;

import android.app.Application;

import androidx.room.Room;

import com.penzhulyak.samplecr.data.AppDataManager;
import com.penzhulyak.samplecr.data.IDataManager;
import com.penzhulyak.samplecr.data.local.db.DataBaseHelper;
import com.penzhulyak.samplecr.data.local.db.IDatabaseHelper;
import com.penzhulyak.samplecr.data.local.db.SampleCRBDatabase;
import com.penzhulyak.samplecr.data.local.db.dao.ChatRoomDao;
import com.penzhulyak.samplecr.data.local.prefs.IPreferencesHelper;
import com.penzhulyak.samplecr.data.local.prefs.PreferencesHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public abstract class StorageModule {
    @Provides
    @Singleton
    static IDataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }

    @Provides
    @Singleton
    static SampleCRBDatabase provideDb(Application context) {
        return Room.databaseBuilder(context.getApplicationContext(), SampleCRBDatabase.class, "crb.db")
                .fallbackToDestructiveMigration()
                .build();
    }

    @Provides
    @Singleton
    static IDatabaseHelper provideDatabaseHelper(DataBaseHelper dataBaseHelper) {
        return dataBaseHelper;
    }

    @Provides
    @Singleton
    static ChatRoomDao provideChatRoomDao(SampleCRBDatabase db) {
        return db.chatRoomDao();
    }

    @Provides
    @Singleton
    static String providePreferenceName() {
        return "sample_crb_pref";
    }

    @Provides
    @Singleton
    static IPreferencesHelper providePreferencesHelper(PreferencesHelper preferencesHelper) {
        return preferencesHelper;
    }
}
