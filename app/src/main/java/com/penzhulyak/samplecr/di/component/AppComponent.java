package com.penzhulyak.samplecr.di.component;

import android.app.Application;

import com.penzhulyak.samplecr.base.BaseApplication;
import com.penzhulyak.samplecr.di.modules.AppModule;
import com.penzhulyak.samplecr.di.modules.StorageModule;
import com.penzhulyak.samplecr.di.modules.UIBindingModule;
import com.penzhulyak.samplecr.di.viewmodel.ViewModelModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import dagger.android.support.DaggerApplication;

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        AppModule.class,
        StorageModule.class,
        ViewModelModule.class,
        UIBindingModule.class
})

public interface AppComponent extends AndroidInjector<DaggerApplication> {

    void inject(BaseApplication baseApplication);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }
}