package com.penzhulyak.samplecr.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatBotDialog {
    @SerializedName("message_id")
    @Expose
    private Long messageId;
    @SerializedName("message_type")
    @Expose
    private Long messageType;
    @SerializedName("bot_message")
    @Expose
    private String botMessage;
    @SerializedName("user_message")
    @Expose
    private String userMessage;

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public Long getMessageType() {
        return messageType;
    }

    public void setMessageType(Long messageType) {
        this.messageType = messageType;
    }

    public String getBotMessage() {
        return botMessage;
    }

    public void setBotMessage(String botMessage) {
        this.botMessage = botMessage;
    }

    public String getUserMessage() {
        return userMessage;
    }

    public void setUserMessage(String userMessage) {
        this.userMessage = userMessage;
    }
}
