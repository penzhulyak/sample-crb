package com.penzhulyak.samplecr.data.local.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.penzhulyak.samplecr.data.model.entity.ChatRoom;

import java.util.List;

@Dao
public interface ChatRoomDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertChatRoom(ChatRoom chatRoom);

    @Query("DELETE FROM Chatroom WHERE chat_room_id = :id")
    void deleteChatRoom(long id);

    @Query("SELECT * FROM ChatRoom")
    LiveData<List<ChatRoom>> getAllChatRooms();

    @Query("SELECT * FROM ChatRoom WHERE chat_room_id = :id")
    LiveData<ChatRoom> getChatRoomById(long id);
}
