package com.penzhulyak.samplecr.data.local.prefs;

public interface IPreferencesHelper {

    String getUser();

    void setUser(String user);

    boolean isLoggedIn();

    void setLoggedIn(boolean isLoggedIn);
}