package com.penzhulyak.samplecr.data;


import com.penzhulyak.samplecr.data.local.db.IDatabaseHelper;
import com.penzhulyak.samplecr.data.local.prefs.IPreferencesHelper;

public interface IDataManager extends IPreferencesHelper, IDatabaseHelper {

}
