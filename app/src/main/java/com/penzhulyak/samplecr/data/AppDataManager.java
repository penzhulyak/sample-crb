package com.penzhulyak.samplecr.data;

import android.content.Context;

import androidx.lifecycle.LiveData;

import com.penzhulyak.samplecr.data.local.db.DataBaseHelper;
import com.penzhulyak.samplecr.data.local.db.IDatabaseHelper;
import com.penzhulyak.samplecr.data.local.prefs.PreferencesHelper;
import com.penzhulyak.samplecr.data.model.entity.ChatRoom;
import com.penzhulyak.samplecr.data.model.entity.User;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class AppDataManager implements IDataManager, IDatabaseHelper {
    private final Context mContext;
    private final PreferencesHelper mPreferencesHelper;
    private final DataBaseHelper mDatabaseHelper;

    @Inject
    AppDataManager(Context context, PreferencesHelper preferencesHelper, DataBaseHelper databaseHelper) {
        mContext = context;
        mPreferencesHelper = preferencesHelper;
        mDatabaseHelper = databaseHelper;
    }

    @Override
    public String getUser() {
        return mPreferencesHelper.getUser();
    }

    @Override
    public void setUser(String user) {
        mPreferencesHelper.setUser(user);
    }

    @Override
    public boolean isLoggedIn() {
        return mPreferencesHelper.isLoggedIn();
    }

    @Override
    public void setLoggedIn(boolean isLoggedIn) {
        mPreferencesHelper.setLoggedIn(isLoggedIn);
    }

    @Override
    public void insertUser(User user) {
        mDatabaseHelper.insertUser(user);
    }

    @Override
    public void deleteChatRoom(long id) {
        mDatabaseHelper.deleteChatRoom(id);
    }

    @Override
    public LiveData<List<User>> getAllUsers() {
        return mDatabaseHelper.getAllUsers();
    }

    @Override
    public LiveData<User> getUserById(int id) {
        return mDatabaseHelper.getUserById(id);
    }

    @Override
    public void isUserValid(String login, String password, IValidationUserCallback callback) {
        mDatabaseHelper.isUserValid(login, password, callback);
    }

    @Override
    public void insertChatRoom(ChatRoom chatRoom) {
        mDatabaseHelper.insertChatRoom(chatRoom);
    }

    @Override
    public LiveData<List<ChatRoom>> getAllChatRooms() {
        return mDatabaseHelper.getAllChatRooms();
    }

    @Override
    public LiveData<ChatRoom> getChatRoomById(long id) {
        return mDatabaseHelper.getChatRoomById(id);
    }
}
