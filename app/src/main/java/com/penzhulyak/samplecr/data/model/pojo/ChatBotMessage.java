package com.penzhulyak.samplecr.data.model.pojo;

public class ChatBotMessage {
    private Long id;
    private boolean isBot;
    private String message;

    public ChatBotMessage(Long id, boolean isBot, String message) {
        this.id = id;
        this.isBot = isBot;
        this.message = message;
    }

    public Long getId() {
        return id;
    }

    public boolean isBot() {
        return isBot;
    }

    public String getMessage() {
        return message;
    }
}
