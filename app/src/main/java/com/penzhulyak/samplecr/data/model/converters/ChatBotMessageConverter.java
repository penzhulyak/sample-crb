package com.penzhulyak.samplecr.data.model.converters;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.penzhulyak.samplecr.data.ChatBotDialog;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

public class ChatBotMessageConverter {
    static Gson gson = new Gson();

    @TypeConverter
    public static List<ChatBotDialog> stringToDialogList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }
        Type listType = new TypeToken<List<ChatBotDialog>>() {
        }.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String dialogListToString(List<ChatBotDialog> objects) {
        return gson.toJson(objects);
    }
}
