package com.penzhulyak.samplecr.data.local.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.penzhulyak.samplecr.data.local.db.dao.ChatRoomDao;
import com.penzhulyak.samplecr.data.local.db.dao.UserDao;
import com.penzhulyak.samplecr.data.model.entity.ChatRoom;
import com.penzhulyak.samplecr.data.model.entity.User;

@Database(entities = {User.class, ChatRoom.class}, version = SampleCRBDatabase.DB_VERSION, exportSchema = false)
public abstract class SampleCRBDatabase extends RoomDatabase {
    static final int DB_VERSION = 1;

    public abstract UserDao userDao();

    public abstract ChatRoomDao chatRoomDao();
}
