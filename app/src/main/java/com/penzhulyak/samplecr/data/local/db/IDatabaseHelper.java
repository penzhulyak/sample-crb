package com.penzhulyak.samplecr.data.local.db;

import androidx.lifecycle.LiveData;

import com.penzhulyak.samplecr.data.model.entity.ChatRoom;
import com.penzhulyak.samplecr.data.model.entity.User;

import java.util.List;

public interface IDatabaseHelper {

    void insertUser(User user);

    void deleteChatRoom(long id);

    LiveData<List<User>> getAllUsers();

    LiveData<User> getUserById(int id);

    void isUserValid(String login, String password, IValidationUserCallback callback);

    void insertChatRoom(ChatRoom chatRoom);

    LiveData<List<ChatRoom>> getAllChatRooms();

    LiveData<ChatRoom> getChatRoomById(long id);

    interface IValidationUserCallback {

        void onUserIsCorrect();

        void onUserIsIncorrect();
    }
}
