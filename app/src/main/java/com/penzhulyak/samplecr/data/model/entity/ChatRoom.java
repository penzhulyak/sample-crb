package com.penzhulyak.samplecr.data.model.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.penzhulyak.samplecr.data.ChatBotDialog;
import com.penzhulyak.samplecr.data.model.converters.ChatBotMessageConverter;

import java.util.List;

@Entity(tableName = "chatroom")
public class ChatRoom {
    @PrimaryKey
    @NonNull
    @SerializedName("chat_room_id")
    @ColumnInfo(name = "chat_room_id")
    private Long mId;

    @SerializedName("chat_room_title")
    @ColumnInfo(name = "chat_room_title")
    private String mTitle;

    @SerializedName("chat_room_dialogs")
    @ColumnInfo(name = "chat_room_dialogs")
    @TypeConverters({ChatBotMessageConverter.class})
    @Expose
    private List<ChatBotDialog> mChatBotDialogs = null;


    @NonNull
    public Long getId() {
        return mId;
    }

    public void setId(@NonNull Long mId) {
        this.mId = mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public List<ChatBotDialog> getChatBotDialogs() {
        return mChatBotDialogs;
    }

    public void setChatBotDialogs(List<ChatBotDialog> mChatBotDialogs) {
        this.mChatBotDialogs = mChatBotDialogs;
    }
}
