package com.penzhulyak.samplecr.data.local.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.penzhulyak.samplecr.data.model.entity.User;

import java.util.List;

@Dao
public interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertUser(User user);

    @Query("SELECT * FROM Users")
    LiveData<List<User>> getAllUsers();

    @Query("SELECT * FROM Users")
    List<User> getUsers();

    @Query("SELECT * FROM Users WHERE id = :id")
    LiveData<User> getUser(int id);
}
