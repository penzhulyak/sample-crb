package com.penzhulyak.samplecr.data.local.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

public class PreferencesHelper implements IPreferencesHelper {

    private static final String PREF_KEY_LOGGED_IN = "PREF_KEY_LOGGED_IN";
    private static final String PREF_KEY_USER = "PREF_KEY_USER";

    private final SharedPreferences mPrefs;

    @Inject
    PreferencesHelper(Context context, String prefFileName) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }

    @Override
    public String getUser() {
        return mPrefs.getString(PREF_KEY_USER, null);
    }

    @Override
    public void setUser(String user) {
        mPrefs.edit().putString(PREF_KEY_USER, user).apply();
    }

    @Override
    public boolean isLoggedIn() {
        return mPrefs.getBoolean(PREF_KEY_LOGGED_IN, false);
    }

    @Override
    public void setLoggedIn(boolean isLoggedIn) {
        mPrefs.edit().putBoolean(PREF_KEY_LOGGED_IN, isLoggedIn).apply();
    }
}
