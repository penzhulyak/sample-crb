package com.penzhulyak.samplecr.data.local.db;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.penzhulyak.samplecr.data.local.db.dao.ChatRoomDao;
import com.penzhulyak.samplecr.data.local.db.dao.UserDao;
import com.penzhulyak.samplecr.data.model.entity.ChatRoom;
import com.penzhulyak.samplecr.data.model.entity.User;
import com.penzhulyak.samplecr.utils.AppExecutors;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class DataBaseHelper implements IDatabaseHelper {

    private final SampleCRBDatabase mAppDatabase;
    private final AppExecutors mAppExecutors;
    private UserDao userDao;
    private ChatRoomDao chatRoomDao;

    @Inject
    public DataBaseHelper(SampleCRBDatabase appDatabase, AppExecutors appExecutors) {
        mAppDatabase = appDatabase;
        mAppExecutors = appExecutors;
        userDao = mAppDatabase.userDao();
        chatRoomDao = mAppDatabase.chatRoomDao();
    }

    @Override
    public LiveData<List<User>> getAllUsers() {
        return userDao.getAllUsers();
    }

    @Override
    public LiveData<User> getUserById(int id) {
        return userDao.getUser(id);
    }

    @Override
    public void isUserValid(String login, String password, IValidationUserCallback callback) {
        Runnable runnable = () -> {
            final List<User> users = userDao.getUsers();
            mAppExecutors.mainThread().execute(() -> {
                for (User user : users) {
                    if (user.getLogin().equals(login) && user.getPassword().equals(password))
                        callback.onUserIsCorrect();
                    else
                        callback.onUserIsIncorrect();
                }
            });
        };
        mAppExecutors.diskIO().execute(runnable);
    }

    @Override
    public void insertChatRoom(ChatRoom chatRoom) {
        Runnable saveRunnable = () -> chatRoomDao.insertChatRoom(chatRoom);
        mAppExecutors.diskIO().execute(saveRunnable);
    }

    @Override
    public LiveData<List<ChatRoom>> getAllChatRooms() {
        return chatRoomDao.getAllChatRooms();
    }

    @Override
    public LiveData<ChatRoom> getChatRoomById(long id) {
        return chatRoomDao.getChatRoomById(id);
    }

    @Override
    public void insertUser(@NonNull final User user) {
        Runnable saveRunnable = () -> userDao.insertUser(user);
        mAppExecutors.diskIO().execute(saveRunnable);
    }

    @Override
    public void deleteChatRoom(long id) {
        Runnable runnable = () -> mAppDatabase.chatRoomDao().deleteChatRoom(id);
        mAppExecutors.diskIO().execute(runnable);
    }
}
