package com.penzhulyak.samplecr.utils;

import androidx.recyclerview.widget.DiffUtil;

import com.penzhulyak.samplecr.data.model.entity.ChatRoom;

import java.util.List;

public class ChatRoomListDiffUtil extends DiffUtil.Callback {

    private List<ChatRoom> mOldChats, mNewChats;

    public ChatRoomListDiffUtil(List<ChatRoom> oldChats, List<ChatRoom> newChats) {
        mOldChats = oldChats;
        mNewChats = newChats;
    }

    @Override
    public int getOldListSize() {
        return mOldChats.size();
    }

    @Override
    public int getNewListSize() {
        return mNewChats.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldChats.get(oldItemPosition).getId() == (mNewChats.get(newItemPosition).getId());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldChats.get(oldItemPosition).getChatBotDialogs().equals(mNewChats.get(newItemPosition).getChatBotDialogs());
    }
}
