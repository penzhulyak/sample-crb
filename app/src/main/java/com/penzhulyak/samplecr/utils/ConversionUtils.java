package com.penzhulyak.samplecr.utils;

import androidx.databinding.BindingConversion;

import com.penzhulyak.samplecr.data.model.entity.User;

import java.util.List;

public class ConversionUtils {

    public ConversionUtils() {

    }

    @BindingConversion
    public static String convertLongToString(long value) {
        return String.valueOf(value);
    }

    @BindingConversion
    public static String convertUserToString(List<User> user) {
        StringBuilder sb = new StringBuilder();
        if (user != null && !user.isEmpty()) {
            sb.append("Use login: '")
                    .append(user.get(0).getLogin())
                    .append("' and password: '")
                    .append(user.get(0).getPassword())
                    .append("' to log in this APP");
        }
        return sb.toString();
    }
}
