package com.penzhulyak.samplecr.utils;

import androidx.recyclerview.widget.DiffUtil;

import com.penzhulyak.samplecr.data.model.pojo.ChatBotMessage;

import java.util.List;

public class ChatRoomMessageListDiffUtil extends DiffUtil.Callback {

    private List<ChatBotMessage> mOldMessages, mNewMessages;

    public ChatRoomMessageListDiffUtil(List<ChatBotMessage> oldMessages, List<ChatBotMessage> newMessages) {
        mOldMessages = oldMessages;
        mNewMessages = newMessages;
    }

    @Override
    public int getOldListSize() {
        return mOldMessages.size();
    }

    @Override
    public int getNewListSize() {
        return mNewMessages.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldMessages.get(oldItemPosition).getId() == (mNewMessages.get(newItemPosition).getId());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldMessages.get(oldItemPosition).getMessage().equals(mNewMessages.get(newItemPosition).getMessage());
    }
}
