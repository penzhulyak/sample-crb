package com.penzhulyak.samplecr.utils;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;

public class BindingUtils {

    public BindingUtils() {

    }

    @BindingAdapter("bind:visibility")
    public static void onVisibility(View view, boolean visibility) {
        view.setVisibility(visibility ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter({"bind:bindImgUrl", "bind:holderImage"})
    public static void onBindImgUrl(ImageView view, String bindImgUrl, Drawable holderImage) {
        Glide.with(view.getContext())
                .load(bindImgUrl)
                .centerCrop()
                .placeholder(holderImage)
                .into(view);
    }

}
