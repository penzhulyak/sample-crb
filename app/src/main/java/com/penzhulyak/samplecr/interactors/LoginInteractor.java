package com.penzhulyak.samplecr.interactors;


import androidx.lifecycle.MutableLiveData;

import com.penzhulyak.samplecr.data.AppDataManager;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class LoginInteractor {

    private final MutableLiveData<Boolean> isLoggedIn = new MutableLiveData<>();
    private AppDataManager mAppDataManager;

    @Inject
    public LoginInteractor(AppDataManager appDataManager) {
        mAppDataManager = appDataManager;
        isLoggedIn.setValue(mAppDataManager.isLoggedIn());
    }


    public MutableLiveData<Boolean> getIsLoggedIn() {
        return isLoggedIn;
    }

    public void setLoggedIn(boolean value) {
        isLoggedIn.setValue(value);
        mAppDataManager.setLoggedIn(value);
    }
}
