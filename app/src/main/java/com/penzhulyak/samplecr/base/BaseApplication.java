package com.penzhulyak.samplecr.base;

import com.penzhulyak.samplecr.di.component.AppComponent;
import com.penzhulyak.samplecr.di.component.DaggerAppComponent;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

public class BaseApplication extends DaggerApplication {

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AppComponent component = DaggerAppComponent.builder()
                .application(this)
                .build();
        component.inject(this);

        return component;
    }
}