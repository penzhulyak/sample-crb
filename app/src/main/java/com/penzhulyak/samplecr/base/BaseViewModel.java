package com.penzhulyak.samplecr.base;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.penzhulyak.samplecr.interactors.LoginInteractor;
import com.penzhulyak.samplecr.interactors.RequestsInteractor;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class BaseViewModel extends ViewModel implements LifecycleOwner {

    protected CompositeDisposable disposable = new CompositeDisposable();
    @Inject
    LoginInteractor loginInteractor;
    @Inject
    RequestsInteractor requestsInteractor;
    private LifecycleRegistry lifecycle = new LifecycleRegistry(this);

    public void setLoggedIn(boolean loggedIn) {
        loginInteractor.setLoggedIn(loggedIn);
    }

    public MutableLiveData<Boolean> isLoggedIn() {
        return loginInteractor.getIsLoggedIn();
    }

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return lifecycle;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        lifecycle.markState(Lifecycle.State.DESTROYED);
        disposable.clear();
    }

    public MutableLiveData<Boolean> isProgressing() {
        return requestsInteractor.isProgressing();
    }

    public void setProgressing(boolean value) {
        requestsInteractor.setProgressing(value);
    }

    public MutableLiveData<String> getMessage() {
        return requestsInteractor.getMessage();
    }

    public void setMessage(String message) {
        requestsInteractor.setMessage(message);
    }
}
