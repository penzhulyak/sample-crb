package com.penzhulyak.samplecr.base;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import dagger.android.support.DaggerFragment;

public class BaseFragment extends DaggerFragment {
    private ActionBar actionbar;

    public ActionBar getActionbar() {
        return actionbar;
    }

    public void setBackArrowActionbar(boolean isShow, String title) {
        actionbar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (isShow) {
            actionbar.setDisplayHomeAsUpEnabled(true);
        } else
            actionbar.setDisplayHomeAsUpEnabled(false);
        actionbar.setDisplayShowHomeEnabled(true);
        actionbar.setTitle(title);
    }
}
