package com.penzhulyak.samplecr.ui.main;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.penzhulyak.samplecr.base.BaseViewModel;
import com.penzhulyak.samplecr.data.AppDataManager;
import com.penzhulyak.samplecr.data.model.entity.ChatRoom;
import com.penzhulyak.samplecr.data.model.entity.User;

import java.util.List;

import javax.inject.Inject;

public class MainViewModel extends BaseViewModel {

    private final MutableLiveData<String> imgUrl = new MutableLiveData<>();
    private AppDataManager mAppDataManager;
    private LiveData<List<ChatRoom>> allChatRooms;
    private LiveData<List<User>> users;

    @Inject
    public MainViewModel(AppDataManager appDataManager) {
        mAppDataManager = appDataManager;
        allChatRooms = mAppDataManager.getAllChatRooms();
        users = mAppDataManager.getAllUsers();
        setImgUrl("https://html.com/wp-content/plugins/htmlcodetutorial-plugin/assets/images/chrome-true.png");
    }

    public LiveData<List<User>> getUsers() {
        return users;
    }

    public void createNewUser() {
        mAppDataManager.insertUser(new User("test", "1234"));
    }

    public void saveChatRoomsToDataBase(ChatRoom chatRoom) {
        mAppDataManager.insertChatRoom(chatRoom);
    }

    public LiveData<List<ChatRoom>> getAllChatRooms() {
        return allChatRooms;
    }

    public MutableLiveData<String> getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String value) {
        imgUrl.setValue(value);
    }
}
