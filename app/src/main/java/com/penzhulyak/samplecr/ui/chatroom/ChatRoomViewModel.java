package com.penzhulyak.samplecr.ui.chatroom;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.penzhulyak.samplecr.base.BaseViewModel;
import com.penzhulyak.samplecr.data.AppDataManager;
import com.penzhulyak.samplecr.data.model.entity.ChatRoom;
import com.penzhulyak.samplecr.data.model.pojo.ChatBotMessage;
import com.penzhulyak.samplecr.utils.SingleLiveEvent;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ChatRoomViewModel extends BaseViewModel {

    private final MutableLiveData<String> answer = new MutableLiveData<>();
    private final MutableLiveData<Boolean> clearAvailable = new MutableLiveData<>();
    private AppDataManager mAppDataManager;
    private MutableLiveData<List<ChatBotMessage>> messages = new MutableLiveData<>();
    private List<ChatRoom> chatRoomList = new ArrayList<>();
    private SingleLiveEvent<Boolean> onAddAnswerClicked = new SingleLiveEvent<>();
    private SingleLiveEvent<Boolean> onSaveChatClicked = new SingleLiveEvent<>();
    private SingleLiveEvent<Boolean> onClearChatClicked = new SingleLiveEvent<>();

    @Inject
    public ChatRoomViewModel(AppDataManager appDataManager) {
        mAppDataManager = appDataManager;
    }

    public LiveData<List<ChatRoom>> getChatRooms() {
        return mAppDataManager.getAllChatRooms();
    }

    public void clearCurrentChat() {
        mAppDataManager.deleteChatRoom(0);
    }

    public List<ChatRoom> getChatRoomList() {
        return chatRoomList;
    }

    public void setChatRoomList(List<ChatRoom> list) {
        chatRoomList = list;
    }

    public MutableLiveData<List<ChatBotMessage>> getMessages() {
        return messages;
    }

    public void setMessages(List<ChatBotMessage> messageList) {
        messages.setValue(messageList);
    }

    public MutableLiveData<String> getAnswer() {
        return answer;
    }

    public void setAnswer(String value) {
        answer.setValue(value);
    }

    public SingleLiveEvent<Boolean> getOnAddAnswerClicked() {
        return onAddAnswerClicked;
    }

    public void setOnAddAnswerClicked(boolean value) {
        onAddAnswerClicked.setValue(value);
    }

    public SingleLiveEvent<Boolean> getOnSaveChatClicked() {
        return onSaveChatClicked;
    }

    public void setOnSaveChatClicked(boolean value) {
        setProgressing(true);
        onSaveChatClicked.setValue(value);
    }

    public SingleLiveEvent<Boolean> getOnClearChatClicked() {
        return onClearChatClicked;
    }

    public void setOnClearChatClicked(boolean value) {
        onClearChatClicked.setValue(value);
    }

    public MutableLiveData<Boolean> getClearAvailable() {
        return clearAvailable;
    }

    public void setClearAvailable(boolean value) {
        clearAvailable.setValue(value);
    }

    public void saveToDataBase(ChatRoom chatRoom) {
        mAppDataManager.insertChatRoom(chatRoom);
        setProgressing(false);
    }
}
