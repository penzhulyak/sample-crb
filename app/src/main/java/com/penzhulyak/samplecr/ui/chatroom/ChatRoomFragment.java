package com.penzhulyak.samplecr.ui.chatroom;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.penzhulyak.samplecr.R;
import com.penzhulyak.samplecr.base.BaseFragment;
import com.penzhulyak.samplecr.data.ChatBotDialog;
import com.penzhulyak.samplecr.data.model.entity.ChatRoom;
import com.penzhulyak.samplecr.data.model.pojo.ChatBotMessage;
import com.penzhulyak.samplecr.databinding.FragmentChatroomBinding;
import com.penzhulyak.samplecr.di.viewmodel.ViewModelFactory;
import com.socks.library.KLog;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ChatRoomFragment extends BaseFragment {

    @Inject
    ViewModelFactory viewModelFactory;
    private ChatRoomViewModel mViewModel;
    private FragmentChatroomBinding binding;
    private ChatRoomAdapter mAdapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_chatroom, container, false);

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(ChatRoomViewModel.class);

        setBackArrowActionbar(true, "Chats");
        setUpRecycler();
        subscribeToViewModel();
        binding.setLifecycleOwner(this);
        binding.setViewModel(mViewModel);
    }

    private void subscribeToViewModel() {
        mViewModel.getChatRooms().observe(this, chatRooms -> {
            mViewModel.setChatRoomList(chatRooms);
            for (ChatRoom chatRoom : chatRooms) {
                KLog.d(chatRoom.toString());
                List<ChatBotDialog> chatBotDialog = chatRoom.getChatBotDialogs();
                List<ChatBotMessage> messages = new ArrayList<>();
                for (ChatBotDialog dilog : chatBotDialog) {
                    ChatBotMessage messageBot = new ChatBotMessage((long) messages.size() + 1,
                            true, dilog.getBotMessage());
                    messages.add(messageBot);
                    if (!dilog.getUserMessage().isEmpty()) {
                        ChatBotMessage messageUser = new ChatBotMessage((long) messages.size() + 1,
                                false, dilog.getUserMessage());
                        messages.add(messageUser);
                    } else break;
                }
                mViewModel.setMessages(messages);
            }
        });
        mViewModel.getMessages().observe(this, chatBotMessages -> {
            KLog.d("need update recycler: " + chatBotMessages.size());
            mAdapter.setItems(chatBotMessages);
        });

        mViewModel.getOnClearChatClicked().observe(this, aBoolean -> mViewModel.clearCurrentChat());

        mViewModel.getOnAddAnswerClicked().observe(this, aBoolean -> createNewAnswer());

        mViewModel.getOnSaveChatClicked().observe(this, aBoolean -> saveChatRoom());
    }

    private void createNewAnswer() {
        List<ChatBotMessage> items = new ArrayList<>();
        items.addAll(mAdapter.getItems());
        ChatBotMessage messageUser = new ChatBotMessage((long) items.size() + 1,
                false, mViewModel.getAnswer().getValue());
        items.add(messageUser);
        mViewModel.setMessages(items);
        mViewModel.setAnswer("");
        new Handler().postDelayed(() -> {
            List<ChatBotDialog> chatBotDialog =
                    mViewModel.getChatRoomList().get(0).getChatBotDialogs();
            if (mAdapter.getItemCount() / 2 < chatBotDialog.size()) {
                ChatBotDialog dialog = chatBotDialog.get(mAdapter.getItemCount() / 2);
                ChatBotMessage messageBot = new ChatBotMessage((long) items.size() + 1,
                        true, dialog.getBotMessage());
                items.add(messageBot);
                mViewModel.setMessages(items);
            } else mViewModel.setClearAvailable(true);
        }, 500);
    }

    private void saveChatRoom() {
        List<ChatRoom> chatBotRoomList = mViewModel.getChatRoomList();
        ChatRoom room = chatBotRoomList.get(0);
        List<ChatBotDialog> dialog = room.getChatBotDialogs();
        List<ChatBotMessage> items = mAdapter.getItems();
        List<ChatBotMessage> answers = new ArrayList<>();
        for (ChatBotMessage message : items) {
            if (!message.isBot())
                answers.add(message);
        }
        for (int i = 0; i < dialog.size(); i++) {
            if (i < answers.size()) {
                dialog.get(i).setUserMessage(answers.get(i).getMessage());
            } else break;
        }
        room.setChatBotDialogs(dialog);
        chatBotRoomList.set(0, room);
        new Handler().postDelayed(() -> {
            mViewModel.saveToDataBase(room);
            Toast.makeText(getActivity(), "ChatRoom saved", Toast.LENGTH_SHORT).show();
        }, 1200);
    }

    private void setUpRecycler() {
        mAdapter = new ChatRoomAdapter();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.recycler.setLayoutManager(layoutManager);
        binding.recycler.setAdapter(mAdapter);
    }
}
