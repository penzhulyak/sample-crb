package com.penzhulyak.samplecr.ui.main;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.penzhulyak.samplecr.base.BaseViewHolder;
import com.penzhulyak.samplecr.data.model.entity.ChatRoom;
import com.penzhulyak.samplecr.databinding.ChatroomItemBinding;
import com.socks.library.KLog;

import java.util.ArrayList;
import java.util.List;

public class MainAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    private List<ChatRoom> mChatRoomList = new ArrayList<>();
    private IItemListener mListener;

    public MainAdapter(IItemListener listener) {
        mListener = listener;
    }

    public void setItems(List<ChatRoom> messages) {
        mChatRoomList.clear();
        mChatRoomList.addAll(messages);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ChatroomItemBinding chatRoomViewHolder = ChatroomItemBinding
                .inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
        return new ViewHolder(chatRoomViewHolder);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        KLog.d(mChatRoomList.size());
        return mChatRoomList != null ? mChatRoomList.size() : 0;
    }

    public class ViewHolder extends BaseViewHolder {
        ChatroomItemBinding binding;

        public ViewHolder(ChatroomItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        @Override
        public void onBind(int position) {
            ChatRoom chatRoom = mChatRoomList.get(position);
            binding.setChat(chatRoom);
            binding.setEvent(mListener);
        }
    }
}
