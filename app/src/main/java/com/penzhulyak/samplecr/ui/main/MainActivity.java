package com.penzhulyak.samplecr.ui.main;

import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.google.gson.Gson;
import com.penzhulyak.samplecr.R;
import com.penzhulyak.samplecr.base.BaseActivity;
import com.penzhulyak.samplecr.data.model.entity.ChatRoom;
import com.penzhulyak.samplecr.databinding.ActivityMainBinding;
import com.penzhulyak.samplecr.di.viewmodel.ViewModelFactory;

import java.io.IOException;
import java.io.InputStream;

import javax.inject.Inject;

public class MainActivity extends BaseActivity {

    @Inject
    ViewModelFactory viewModelFactory;

    private NavController navController;
    private ActivityMainBinding binding;
    private MainViewModel mViewModel;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel.class);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        setFullScreenMode(false);

        mViewModel.isLoggedIn().observe(this, aBoolean -> {
            if (aBoolean) {
                setFullScreenMode(false);
                navController.navigate(R.id.action_global_mainFragment);
            } else {
                setFullScreenMode(true);
                navController.navigate(R.id.action_mainFragment_to_loginFragment);
            }
        });

        mViewModel.getUsers().observe(this, users -> {
            if (users.isEmpty())
                mViewModel.createNewUser();
        });
        mViewModel.getAllChatRooms().observe(this, chatRooms -> {
            if (chatRooms.isEmpty()) {
                ChatRoom cr = new Gson().fromJson(loadJSONFromAsset(), ChatRoom.class);
                mViewModel.saveChatRoomsToDataBase(cr);
            }
        });

        binding.setLifecycleOwner(this);
        binding.setViewModel(mViewModel);
    }

    @Override
    public void onBackPressed() {
        navController.popBackStack();
    }

    public String loadJSONFromAsset() {
        String json;
        try {
            InputStream is = getAssets().open("chatroom.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
