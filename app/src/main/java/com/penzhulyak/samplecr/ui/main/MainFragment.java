package com.penzhulyak.samplecr.ui.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.penzhulyak.samplecr.R;
import com.penzhulyak.samplecr.base.BaseFragment;
import com.penzhulyak.samplecr.data.model.entity.ChatRoom;
import com.penzhulyak.samplecr.databinding.FragmentMainBinding;
import com.penzhulyak.samplecr.di.viewmodel.ViewModelFactory;

import java.util.List;

import javax.inject.Inject;

public class MainFragment extends BaseFragment {

    NavController navController;
    @Inject
    ViewModelFactory viewModelFactory;
    private ActionBar actionbar;
    private MainViewModel mViewModel;
    private FragmentMainBinding binding;
    private MainAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false);
        navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel.class);

        setBackArrowActionbar(false, "ChatRooms");
        subscribeToViewModel();

        setUpRecycler();
        mViewModel.getAllChatRooms().observe(this, new Observer<List<ChatRoom>>() {
            @Override
            public void onChanged(List<ChatRoom> chatRooms) {
                if (!chatRooms.isEmpty())
                    mAdapter.setItems(chatRooms);
            }
        });

        binding.setLifecycleOwner(this);
        binding.setViewModel(mViewModel);
    }

    private void setUpRecycler() {
        mAdapter = new MainAdapter(viev ->
                navController.navigate(R.id.action_mainFragment_to_chatRoomFragment));
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.recycler.setLayoutManager(layoutManager);
        binding.recycler.setAdapter(mAdapter);
    }

    private void subscribeToViewModel() {

    }
}
