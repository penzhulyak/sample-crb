package com.penzhulyak.samplecr.ui.chatroom;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListUpdateCallback;
import androidx.recyclerview.widget.RecyclerView;

import com.penzhulyak.samplecr.base.BaseViewHolder;
import com.penzhulyak.samplecr.data.model.pojo.ChatBotMessage;
import com.penzhulyak.samplecr.databinding.MessageBotItemBinding;
import com.penzhulyak.samplecr.databinding.MessageUserItemBinding;
import com.penzhulyak.samplecr.utils.ChatRoomMessageListDiffUtil;
import com.socks.library.KLog;

import java.util.ArrayList;
import java.util.List;

public class ChatRoomAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    public static final int MESSAGE_TYPE_BOT = 0;
    public static final int MESSAGE_TYPE_USER = 1;

    private List<ChatBotMessage> mMesageList;
    private ListUpdateCallback listUpdateCallback = new ListUpdateCallback() {
        @Override
        public void onInserted(int position, int count) {
            KLog.d("inserted: " + position + " " + count);
            notifyItemRangeInserted(position, count);
        }

        @Override
        public void onRemoved(int position, int count) {
            KLog.d("removed: " + position + " " + count);
            notifyItemRangeRemoved(position, count);
        }

        @Override
        public void onMoved(int fromPosition, int toPosition) {
            KLog.d("moved: " + fromPosition + " " + toPosition);
            notifyItemMoved(fromPosition, toPosition);
        }

        @Override
        public void onChanged(int position, int count, Object payload) {
            KLog.d("changed: " + position + " " + count + " " + payload);
            notifyItemRangeChanged(position, count);
        }
    };

    public ChatRoomAdapter() {
        mMesageList = new ArrayList<>();
    }

    public List<ChatBotMessage> getItems() {
        return mMesageList;
    }

    public void setItems(List<ChatBotMessage> messages) {
        DiffUtil.DiffResult result = DiffUtil
                .calculateDiff(new ChatRoomMessageListDiffUtil(mMesageList, messages), false);
        mMesageList.clear();
        mMesageList.addAll(messages);
        result.dispatchUpdatesTo(listUpdateCallback);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case MESSAGE_TYPE_BOT:
                MessageBotItemBinding botViewHolder = MessageBotItemBinding
                        .inflate(LayoutInflater.from(parent.getContext()),
                                parent, false);
                return new BotViewHolder(botViewHolder);
            case MESSAGE_TYPE_USER:
            default:
                MessageUserItemBinding userViewHolder = MessageUserItemBinding
                        .inflate(LayoutInflater.from(parent.getContext()),
                                parent, false);
                return new UserViewHolder(userViewHolder);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (mMesageList.get(position).isBot()) {
            return MESSAGE_TYPE_BOT;
        } else {
            return MESSAGE_TYPE_USER;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        KLog.d(mMesageList.size());
        return mMesageList != null ? mMesageList.size() : 0;
    }

    public class BotViewHolder extends BaseViewHolder {
        MessageBotItemBinding binding;

        public BotViewHolder(MessageBotItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        @Override
        public void onBind(int position) {
            ChatBotMessage chatBotMessage = mMesageList.get(position);
            binding.setMessage(chatBotMessage);
        }
    }

    public class UserViewHolder extends BaseViewHolder {
        MessageUserItemBinding binding;

        public UserViewHolder(MessageUserItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        @Override
        public void onBind(int position) {
            ChatBotMessage chatBotMessage = mMesageList.get(position);
            binding.setMessage(chatBotMessage);
        }
    }
}
