package com.penzhulyak.samplecr.ui.login;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.penzhulyak.samplecr.R;
import com.penzhulyak.samplecr.base.BaseFragment;
import com.penzhulyak.samplecr.databinding.FragmentLoginBinding;
import com.penzhulyak.samplecr.di.viewmodel.ViewModelFactory;

import javax.inject.Inject;

public class LoginFragment extends BaseFragment {

    NavController navController;


    @Inject
    ViewModelFactory viewModelFactory;
    private LoginViewModel mViewModel;
    private FragmentLoginBinding binding;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(LoginViewModel.class);
        navController = Navigation.findNavController(binding.getRoot());

        setBackArrowActionbar(false, "Main Activity");

        mViewModel.getMessage().observe(this, s ->
                Toast.makeText(getActivity(), s, Toast.LENGTH_SHORT).show());

        binding.setLifecycleOwner(this);
        binding.setViewModel(mViewModel);
    }


}
