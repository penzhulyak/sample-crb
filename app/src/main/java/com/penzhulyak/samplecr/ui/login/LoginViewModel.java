package com.penzhulyak.samplecr.ui.login;

import android.os.Handler;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.penzhulyak.samplecr.base.BaseViewModel;
import com.penzhulyak.samplecr.data.AppDataManager;
import com.penzhulyak.samplecr.data.local.db.IDatabaseHelper;
import com.penzhulyak.samplecr.data.model.entity.User;

import java.util.List;
import java.util.Random;

import javax.inject.Inject;

public class LoginViewModel extends BaseViewModel {

    private final MutableLiveData<String> login = new MutableLiveData<>();
    private final MutableLiveData<String> password = new MutableLiveData<>();
    private final LiveData<List<User>> user;

    private AppDataManager mAppDataManager;


    @Inject
    public LoginViewModel(AppDataManager appDataManager) {
        mAppDataManager = appDataManager;
        user = mAppDataManager.getAllUsers();
    }

    public LiveData<List<User>> getUser() {
        return user;
    }

    public void setLoginHandler(boolean value) {
        Random r = new Random();
        int delay = r.nextInt(2500 - 1000) + 1000;
        setProgressing(true);
        new Handler().postDelayed(() ->
                mAppDataManager.isUserValid(login.getValue(),
                        password.getValue(),
                        new IDatabaseHelper.IValidationUserCallback() {
                            @Override
                            public void onUserIsCorrect() {
                                setLoggedIn(value);
                                setProgressing(false);
                            }

                            @Override
                            public void onUserIsIncorrect() {
                                setMessage("Incorrect login or password");
                                setProgressing(false);
                            }
                        }), delay);
    }

    public MutableLiveData<String> getLogin() {
        return login;
    }

    public void setLogin(String value) {
        login.setValue(value);
    }

    public MutableLiveData<String> getPassword() {
        return password;
    }

    public void setPassword(String value) {
        password.setValue(value);
    }
}
